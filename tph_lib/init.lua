tph_lib = {}
local scriptname = "tph_lib"
-- NOTES:: REDO QPRINT (DOES NOT CONSIDER STRINGED INDEXES IN UNPACK() ) | TABLE.REVERSE (IS NOT REVERSING TABLES WHY??)
local random = math.random()

function tph_lib.qprint_raw(...)
  -- PARAMETERS: "..." - tuples - will use all values (parameters) provided
  -- RETURNS: string - formatted string ready to be "printed"
  -- FUNCTION: creates a single formatted string of all of the specified arguments
  ------------------------------------------------------------------------------------------------------------------
  
  local function pos_is_valid(pos) -- tph_lib.pos_is_valid built into qprint
    local returnFuncVal = false -- to return

    if (type(pos) == "table") then -- checks if "pos" is a table
      if (type(pos.x) == "number" and type(pos.y) == "number" and type(pos.z) == "number") then -- checks if x, y, and z exist, and if they're numbers
        returnFuncVal = true -- if pos is a table and the numbers are numbers, it's a go!
      end
    end

    return returnFuncVal -- return true or false
  end
  
  local printStr = "" -- declared string variable so that I can .. (concatenate) the (...) to print
  
  for index = 1, select("#",...), 1 do -- "index" to well, index for the total length of the provided tuples/argument (...), checking by 1
    local item = select(index,...) -- get the indexed item of the tuples argument
    
    -- Special conditions for variables (to be edited for the print) \/\/\/
    if (type(item) == "nil") then -- Special Condition : "nil" ;; if nil then turn it into a string, "nil" (minetest doesn't print out nils for some reason)
      item = "nil" -- setting indexed item as "nil"
    elseif (pos_is_valid(item) == true) then -- Special Condition : "pos" ;; automatically print the numbers of a pos table if pos_is_valid finds the given argument to be a pos
      item = ( tostring(value.x)..","..tostring(value.y)..","..tostring(value.z) ) -- getting the x,y,z of a possible pos and stringing them together for a print
    elseif (type(item) == "table") then
      local function stringify(argt) -- turn a table into a string :D
        local STC = "" -- StringedTableContents
        
        for ti,tv in pairs(argt) do -- iterate through the contents of the table
          if (STC == "") then -- if STC hasn't been modified
            STC = STC.." ["..tostring(ti).."]="..tostring(tv) -- set a string with space
            -- tostring index in [] and tostring value afterwards with a "=" -- [index]=value
          else -- if STC has been modified
            STC = STC..",["..tostring(ti).."]="..tostring(tv) -- set a string with comma
          end
          if (type(tv) == "table") then -- if there's a table inside the table...
            STC = STC..stringify(tv) -- stringify that table! (and add it too)
          end
        end
        
        STC = "::{"..STC.." }" -- contain table with indexes and values inside a "::{ indexvalues }"
        
        return STC
      end
      
      item = tostring(item)..stringify(item) -- convert table into a string (get its id) and combine with returned stringify
    end
    -- Special conditions for variables (to be edited for the print) /\/\/\
    
    if (printStr == "") then -- if nothing was declared yet then don't add a space
      printStr = tostring(item)
    else
      printStr = ( printStr .. "  " .. (tostring(item)) ) -- converts given arguments into string and concatenates them together (separated by a space)
    end
  end
  
  return printStr
end

function tph_lib.qprint(...) -- quick makeshift print ("q"uick "print")
  -- PARAMETERS: "..." - tuples - will use all values (parameters) provided
  -- RETURNS: nil
  -- FUNCTION: prints to Minetest chat, "action" log, and print(), whatever is provided. Auto converts any provided "positions" into a more readable format
  ------------------------------------------------------------------------------------------------------------------
  --[[
  local function pos_is_valid(pos) -- tph_lib.pos_is_valid built into qprint
    local returnFuncVal = false -- to return

    if (type(pos) == "table") then -- checks if "pos" is a table
      if (type(pos.x) == "number" and type(pos.y) == "number" and type(pos.z) == "number") then -- checks if x, y, and z exist, and if they're numbers
        returnFuncVal = true -- if pos is a table and the numbers are numbers, it's a go!
      end
    end

    return returnFuncVal -- return true or false
  end
  
  local printStr = "" -- declared string variable so that I can .. (concatenate) the (...) to print
  
  for index = 1, select("#",...), 1 do -- "index" to well, index for the total length of the provided tuples/argument (...), checking by 1
    local item = select(index,...) -- get the indexed item of the tuples argument
    
    -- Special conditions for variables (to be edited for the print) \/\/\/
    if (type(item) == "nil") then -- Special Condition : "nil" ;; if nil then turn it into a string, "nil" (minetest doesn't print out nils for some reason)
      item = "nil" -- setting indexed item as "nil"
    elseif (pos_is_valid(item) == true) then -- Special Condition : "pos" ;; automatically print the numbers of a pos table if pos_is_valid finds the given argument to be a pos
      item = ( tostring(value.x)..","..tostring(value.y)..","..tostring(value.z) ) -- getting the x,y,z of a possible pos and stringing them together for a print
    end
    -- Special conditions for variables (to be edited for the print) /\/\/\
    
    if (printStr == "") then -- if nothing was declared yet then don't add a space
      printStr = tostring(item)
    else
      printStr = ( printStr .. "  " .. (tostring(item)) ) -- converts given arguments into string and concatenates them together (separated by a space)
    end
  end
  --]]
  local printStr = tph_lib.qprint_raw(...)
  print(printStr) -- "print()" of it as well
  minetest.log("action",printStr) -- "minetest.log()" of it as well
  minetest.chat_send_all(printStr) -- prints into chat for all players to see, the provided print arguments
end

local qprint = tph_lib.qprint -- me defining it locally as "qprint" - "quick print" cause lazy

function tph_lib.is_node_in_group(node, group) -- check if node is in group (I reinvented get_item_group whoops)
  -- PARAMETERS: node - table for minetest nodes - to verify if a specified node is in said group - node can be a "position" | group - string of wanted group's name
  ------------------------------------------------------------------------------------------------------------------
  
  local returnVal = {false}
  -- DOCUMENT
  if (type(node) == "table") then -- if provided node is a table then
    if (tph_lib.pos_is_valid(node) == true) then -- if node is pos
      node = minetest.get_node(node) -- get node from pos
      if (type(node) == "table") then -- if a node is returned
        node = node.name -- get node's name
      end
    elseif (type(node.name) == "string") then -- if node table has .name and it's a string then
      node = node.name -- get node's name
    end
  end
  
  if (type(node) == "string" and type(group) == "string") then
    local nodeTable = minetest.registered_nodes[node]
    if (type(nodeTable) == "table") then
      if (type(nodeTable.groups) == "table") then
        if (type(nodeTable.groups[group]) == "number") then
          returnVal = {true, nodeTable.groups[group]}
        end
      end
    end
  end
  
  return unpack(returnVal)
end

-- "pos" functions \/\/

function tph_lib.pos_is_valid(pos) -- checks if the "pos" is a valid "pos" with x, y, and z and are numbers
  -- PARAMETERS: pos;"position" - table provided to verify it is a "position"
  -- RETURNS: boolean -- true or false depending on if the "position" is verified
  -- FUNCTION: tells whether or not a provided table has "x, y, z" provided as numbers (definition of a pos)
  ------------------------------------------------------------------------------------------------------------------
  
  local returnVal = false -- to return
  
  if (type(pos) == "table") then -- checks if "pos" is a table
    if (type(pos.x) == "number" and type(pos.y) == "number" and type(pos.z) == "number") then -- checks if x, y, and z exist, and if they're numbers
      returnVal = true -- if pos is a table and the numbers are numbers, it's a go!
    end
  end
  
  return returnVal -- return true or false
end

function tph_lib.add_pos(...) -- can also use vector.add(pos,inc/pos)/vector.subtract(pos,inc/pos)
  ------------------------------------------------------------------------------------------------------------------
  local XYZ = {["x"] = math.huge,["y"] = math.huge,["z"] = math.huge}
  -- DOCUMENT
  local contents = {...}
  
  for _,pos in pairs(contents) do
    if (tph_lib.pos_is_valid(pos) == true) then
      if (XYZ.x >= math.huge) then
        XYZ.x = pos.x
      else
        XYZ.x = XYZ.x + pos.x
      end
      if (XYZ.y >= math.huge) then
        XYZ.y = pos.y
      else
        XYZ.y = XYZ.y + pos.y
      end
      if (XYZ.z >= math.huge) then
        XYZ.z = pos.z
      else
        XYZ.z = XYZ.z + pos.z
      end
    end
  end
  
  return XYZ
end

function tph_lib.get_pos_distance(pos1,pos2) -- get distance between two provided positions (can use vector.distance() instead)
  -- PARAMETERS: pos1;"position" - table specifying a x,y,z | pos2;"position" - another table specifying a x,y,z
  -- RETURNS: number - distance between the two specified pos'
  -- FUNCTION: serves as a tph_lib version of vector.distance() - calculates the distance between two pos'
  ------------------------------------------------------------------------------------------------------------------
  
  if (tph_lib.pos_is_valid(pos1) == true and tph_lib.pos_is_valid(pos2) == true) then -- if provided pos' are valid then
    
    local x = 0 -- total difference of X between both pos'
    local y = 0 -- total difference of Y between both pos'
    local z = 0 -- total difference of Z between both pos'
    
    x = ( math.max(pos1.x,pos2.x) - math.min(pos1.x,pos2.x) ) 
    y = ( math.max(pos1.y,pos2.y) - math.min(pos1.y,pos2.y) ) 
    z = ( math.max(pos1.z,pos2.z) - math.min(pos1.z,pos2.z) ) 
-- /\/\/\
-- math.max and math.min are used to determine which are the largest or smalelr numbers, as to appropriately subtract them (bigger number being subtracted by smaller number)
    
    return (x + y + z) -- calculated collective distance of x, y, and z of between provided pos'
  end
end

function tph_lib.get_nodes_and_pos_in_area(pos1,pos2,ignoreContent,getContent,ignorePos) -- checks for nodes in a given area/volume ;; table pos1 | table pos2 {both pos' are necessary and are utilized to find nodes within the area} | table ignoreContent {accepts "groups" (provide a "groups" table ; if value of group is 1, it will ignore all instances with the group declared, if not 1, it will only ignore instances with the group level) and node names - avoid these nodes} | table getContent {accepts "groups" (same notions as ignoreContent, but reversed) and node names - ONLY get these nodes and ignore all others} | table ignorePos {table containing pos' for the function to avoid}
  
  -- returns the node and pos of the node in a table inside a table : table nodesdata = { {node,pos}, {node,pos} }
  ------------------------------------------------------------------------------------------------------------------
  
  local nodedata = {} -- returning this
  
  if (type(ignoreContent) ~= "table") then -- if ignoreContent isn't a table
    ignoreContent = {} -- convert into table for the function
  end
  if (type(getContent) ~= "table") then -- if getContent isn't a table
    getContent = {} -- convert into table for the function
  end
  if (type(ignorePos) ~= "table") then -- if ignorePos isn't a table
    ignorePos = {} -- convert into table for the function
  end
  
  if (tph_lib.pos_is_valid(pos1) == true and tph_lib.pos_is_valid(pos2) == true) then -- checking if both provided pos' are valid
    
    for index,num in pairs(pos1) do -- incase someone tries to do a sneaky with pos1 and give my poor function a decimal
      pos1[index] = math.ceil(num) -- convert into an integer (I know minetest.tointerger() exists but not trying it)
    end
    for index,num in pairs(pos2) do -- incase someone tries to do a sneaky with pos2 and give my poor function a decimal
      pos2[index] = math.ceil(num) -- convert into an integer
    end
    
    local area = ((pos1.x - pos2.x) * (pos1.z - pos2.z)) -- pointlessly calculate the area on a 2D plane with X and Z
    local volume = ((pos1.x - pos2.x) * (pos1.y - pos2.y) * (pos1.z - pos2.z)) -- pointlessly calculate the volume
    local posDif = {["x"] = (pos1.x - pos2.x),["y"] = (pos1.y - pos2.y),["z"] = (pos1.z - pos2.z)} -- differences of the X, Y, Z units between pos1 and pos2
    
    for index,num in pairs(posDif) do -- checker to ensure numbers are not negative integers
      if (num < 0) then -- if a negative integer...
        posDif[index] = -num -- convert into a positive one! (-num is a handy tool, thanks lua)
      end
    end
    
    if (area < 0) then -- if area is somehow negative, make it positive!
      area = -area
    end
    if (volume < 0) then -- if volume is somehow negative, make it positive!
      volume = -volume
    end
    
    -- now to shift through each unit of position...
    for xi = 1, posDif.x, 1 do -- check through every unit of X
      local x = 0 -- used to provide a pos unit of X for each found node
      
      if (pos1.x < pos2.x) then -- if pos1.x is less than pos2.x then
        x = pos2.x - xi -- ensure that pos2.x gets subtracted as a countdown (with xi being the countdown 'til posDif.x is reached)
      else -- if pos1.x is NOT less than pos2.x then
        x = pos1.x - xi
      end
      for yi = 1, posDif.y, 1 do -- check through every unit of Y while going through every unit of X
        local y = 0 -- used to provide a pos unit of Y for each found node
        
        if (pos1.y < pos2.y) then -- if pos1.y is less than pos2.y then
          y = pos2.y - yi
        else -- if pos1.y is NOT less than pos2.y then
          y = pos1.y - yi
        end
        for zi = 1, posDif.z, 1 do -- check through every unit of Z while going through every unit of Y
          local z = 0 -- used to provide a pos unit of Z for each found node
          
          if (pos1.z < pos2.z) then -- if pos1.z is less than pos2.z then
            z = pos2.z - zi
          else -- if pos1.z is NOT less than pos2.z then
            z = pos1.z - zi
          end
          
          local newpos = {["x"] = x,["y"] = y,["z"] = z} -- create a pos with the provided x, y, z
          local gotnode = minetest.get_node(newpos) -- find the node at the created pos
          
          -- ignoreContent aka REFUSE ANY node that maintains any of the checked values
          if (gotnode ~= nil) then -- ensure the node exists before inquiring
            for _,ignore in pairs(ignoreContent) do
              --qprint(ignore,#ignoreContent)
              if (type(ignore) == "string") then -- if thing to ignore is a string then suspect it to be a node's name
                if (gotnode.name == string.lower(ignore)) then -- lowercase the string to get a better result! if to ignore node's name is equal to the grabbed node, then nil it, remove it!
                  gotnode = nil -- make sure it does not get added to the list
                end
              elseif (type(ignore) == "table") then -- if thing to ignore is a table, then suspect it to be a "group" table
                for groupName,value in pairs(ignore) do -- check through provided "groups" table
                  local isInGroup, groupValue = tph_lib.is_node_in_group(gotnode,groupName) -- check if node is in group and get its ranking
                  
                  if (isInGroup == true) then -- if node is in group then
                    if (value == 1) then -- if provided ignore group value is 1 then
                      gotnode = nil -- ignore node
                    elseif (value == groupValue) then -- if provided ignore group value is NOT 1 but is still equal to the node's groups' value then
                      gotnode = nil -- ignore node
                    end
                  end
                end
                
              end
            end
          end
          
          -- getContent aka ONLY ACCEPT noted node if parameters are equal to
          if (gotnode ~= nil) then -- I'm going to do this a third time because I can't get over having if checks EVERYWHERE, I  HAVE TRAUMA OK. Anyways, this is to ensure gotnode hasn't been eliminated by ignoreContent before proceeding to getContent
            if (#getContent > 0) then -- ONLY nullify other nodes IF getContent is specified
              local acceptable = false -- used to determine if the gotnode should be returned ;; true = return node | false = do not return node
              
              for _,getNode in pairs(getContent) do
                if (type(getNode) == "string") then -- if thing to accept is a string, then suspect it to be a node's name
                  if (gotnode.name == string.lower(getNode)) then -- lowercase the string to get a bettered result! if acceptable node name is equal to the grabbed node, then return it!
                    acceptable = true
                  end
                elseif (type(getNode) == "table") then
                  for groupName,value in pairs(ignore) do -- check through provided "groups" table
                    local isInGroup, groupValue = tph_lib.is_node_in_group(gotnode,groupName) -- check if node is in group and get its ranking
                    
                    if (isInGroup == true) then -- if node is in group then
                      if (value == 1) then -- if provided accept group value is 1 then
                        acceptable = true -- accept it!
                      elseif (value == groupValue) then -- if provided accept group value is NOT 1 but still equal to the node's groups' value then
                        acceptable = true -- accept that node!
                      end
                    end
                  end
                
                end
              end
              
              if (acceptable == false) then -- after all checks, if node cannot be accepted then
                gotnode = nil -- ignore that node!
              end
              
            end
          end
          
          -- ignorePos aka IGNORE ANY node that maintains any provided pos within table
          if (gotnode ~= nil) then -- if the node is still returnable, check those pos'!
            for _,ignore in pairs(ignorePos) do
              if (tph_lib.pos_is_valid(ignore) == true) then -- if table of given pos' are valid pos' then
                if (ignore.x == newpos.x and ignore.y == newpos.y and ignore.z == newpos.z) then -- made it check the pos units individually incase someone was a little lazy with their x,y,z and had them mixed up like z,x,y. Anyways, if ignoring pos is equal to the node's pos then
                  --qprint("remove this node! ",newpos," at ",ignore)
                  gotnode = nil -- ignore node!
                end
              end
            end
          end
          
          -- finally, if everything went to plan...
          if (gotnode ~= nil) then -- if gotnode does not equal nil then
            --qprint("in base function",#gotnode,unpack(gotnode))
            local gotnodeLength = 0 -- get length of gotnode to determine if it's empty or not
            for num,_ in pairs(gotnode) do -- checking throuble table to get length because # operator only works if the index is numbered! (no strings allowed! ): )
              gotnodeLength = gotnodeLength + 1 -- add 1 to every index found
            end
            
            if (gotnodeLength > 0) then -- in case function tries to return an empty table for some reason
              nodedata[#nodedata + 1] = {node = gotnode,pos = newpos} -- created a table with gotnode as [1] and newpos as [2], at the end of nodedata
            end
          end
        end
      end
    end
  end
  
  return nodedata -- return that big table of even more tables!
end

-- "pos" functions /\/\


-- "TABLE" SECTION FUNCTIONS

tph_lib.table = {} -- setting for "TABLE" section functions

function tph_lib.table.length(t) -- as the # operator and similar will only get the length of all indexes that are numbered, and not if the indexes are string
  -- PARAMETERS: t;"table" - table to get length of
  -- RETURNS: number - length of table
  -- FUNCTION: iterates through a table and increases a returned value according to every noted iteration through the table
  ------------------------------------------------------------------------------------------------------------------
  
  if (type(t) == "table") then -- check if provided "t" is a table
    local length = 0 -- declared number to use for length
    
    for _,_ in pairs(t) do -- iterate through the table, purposefully blank index and value as they are not necessary
      length = length + 1 -- increase "length" by 1 for every iteration
    end
    
    return length
  else -- if there isn't a table provided...
    error(scriptname..": no table provided for table.length") -- error and provide message if no table is provided
  end
end

function tph_lib.table.clone(table,deep) -- Minetest's "table.copy" is probably better to use lol (this function "deep copies" anyways)
  -- PARAMETERS: "table" - table to clone | deep;"table" -- boolean to determine if a deep or shallow copy should be made (deep = every table inside and their contents cloned | shallow = every table's content inside is NOT cloned and will reference to the old table if indexed)
  -- RETURNS: table - cloned table
  -- FUNCTION: clones a provided table and returns it
  ------------------------------------------------------------------------------------------------------------------
  
  if (type(deep) ~= "boolean") then
    deep = false
  end
  
  local function cloneTable(t) -- locally defined "cloneTable" function as to continuously call it for every table encountered to clone
    local newT = {} -- new table to clone to (locally)
    
    for i,v in pairs(t) do -- iterating over provided table
      if (type(v) == "table") then -- if it finds a value that is a table
        if (deep == true) then -- if committing to a deep copy
          v = cloneTable(v) -- then clone it too!
        end
      end
      newT[i] = v -- cloning to new table - by reusing the old table's index for the new table and adding the corresponding value
    end
    
    return newT
  end
  
  if (type(table) == "table") then -- check if provided "table" is a table
    return cloneTable(table) -- new table that will be made a clone of provided table
  else -- if there isn't a table provided...
    error(scriptname..": no table provided for table.clone") -- error and provide message if no table is provided
  end
end

function tph_lib.table.reverse(t) -- no noted functions for reversing a table so
  -- PARAMETERS: t;"table" - table to reverse
  -- RETURNS: table - reversed table
  -- FUNCTION: returns a reversed table of the provided table - including "dictionaries"
  ------------------------------------------------------------------------------------------------------------------
  
  if (type(t) == "table") then
    local reorder = {}
    
    for i,v in pairs(t) do -- iterate through provided table
      table.insert(reorder,0,{i,v}) -- create a packaged table that will provide the provided table's index as its first key, and the corresponding value as its second. Insert at pos "0" to ensure it is set first and push other packaged tables further down, as to artificially reverse the order to reorder it in the next loop
    end
    
    local t2 = {} -- create a new table to use
    
    for i,v in pairs(reorder) do
      if (type(v) == "table") then -- ensure that the reversed content was correctly packaged and proceed
        if (#v == 2) then -- ditto to last note
          t2[v[1]] = v[2] -- set the index in the reversed order as indicated by "reorder" with the corresponding value
        end
      end
    end
    
    return t2
  else -- if there isn't a table provided...
    error(scriptname..": no table provided for table.reverse") -- error and provide message if no table is provided
  end
end

function tph_lib.table.find(t,value,index)
  -- PARAMETERS: t;"table" - table to use to search through | value;"variant" - value to look for in table | index;"number" (OPTIONAL) - index to start from while looking in table - use negative numbers to look backwards from the point - use table to identify a "range"
  -- RETURNS: boolean - if value is found inside table, return true | variant - "index" value of provided table that the value was found with
  -- FUNCTION: finds a specified value inside of a table
  ------------------------------------------------------------------------------------------------------------------
  
  if (type(t) == "table") then
    if (type(index) == "number") then -- if only 1 numbered index is specified
      index = math.ceil(index) -- convert into integer
      
      index = {index,index} -- convert into a table for next line of code to use
    end
    if (type(index) == "table") then
      -- assume index is 2 in length, and that both of its provided content are numbers
      local i1 = index[1] -- assume the 1st index of "index" is the desired minimum (start) number
      local i2 = index[2] -- assume the 2nd index of "index" is the desired max (end) number
      
      i1 = math.ceil(i1) -- convert into integers
      i2 = math.ceil(i2)
      
      local tlen = tph_lib.table.length(t)
      
      local indexcount = 0 -- used for counting in tables (since string'd indexes cause problems...)
      
      if (type(i1) == "number" and type(i2) == "number" and type(tlen) == "number") then -- if both table'd indexes are numbers, and a table length can be grabbed then
        if (i1 ~= i2) then -- if two indexes were provided (i1 ~= i2 which means 2 provided indexes)
          if ( (i1 >= 0 and i2 <= 0) or (i1 <= 0 and i2 >= 0) ) then
            i2 = -i2
          end
          
          if (i1 >= 0 and i2 >= 0) then -- if both indexes are above or equal to 0, then look through normally
            for i = i1, i2, 1 do -- start at i1, go up until i2
              if (t[i] == value) then -- if value found
                return unpack{true,i} -- return true and return 'i' - index that value was found in
              end
            end
          elseif (i1 < 0 and i2 < 0) then -- if both indexes are lower than 0, look backwards
            for i = -i2, -i1, -1 do -- start at i2, go down until i1
              if (t[i] == value) then -- if value found
                return unpack{true,i} -- return true and return 'i' - index that value was found in
              end
            end
          end
        else -- if only 1 numbered index was specified (i1 == i2 - which means only 1 provided index)
          if (i1 >= 0) then -- if i is greater than or equal to 0, look through table progressively
            for i,v in pairs(t) do
              indexcount = indexcount + 1
              if (indexcount >= i1) then
                if (v == value) then
                  return unpack{true,i}
                end
              end
            end
          elseif (i1 < 0) then -- if i is lesser than 0, look through table regressively
            qprint("i1 less than 0")
            local t2 = tph_lib.table.reverse(t)
            qprint(unpack(t2))
            indexcount = tlen
            
            for i,v in pairs(t) do
              indexcount = indexcount - 1
              if (indexcount <= i1) then
                if (v == value) then
                  return unpack{true,i}
                end
              end
            end
          end
        end
      end
    
    else -- no (OPTIONAL) index provided
      for i,v in pairs(t) do -- iterate through table from start to finish
        if (v == value) then -- if value is found inside provided table
          return unpack{true,i} -- return true and return 'i' - index that value was found in
        end
      end
      
      return unpack{false} -- return false if fully iterated through table without break (value not found)
    end
  else -- if there isn't a table provided...
    error(scriptname..": no table provided for table.find") -- error and provide message if no table is provided
  end
end

-- "TABLE" SECTION FUNCTIONS

-- "MATH" SECTION FUNCTIONS

tph_lib.math = {}

function tph_lib.math.clamp(num,min,max)
  -- PARAMETERS: num;"number" - number to be clamped | min;"number" - minimum number that 'num' can be | max;"number" - maximum number that 'num' can be
  -- RETURNS: number - 'num' that is clamped (or not if 'num' is between 'min' and 'max')
  -- FUNCTION: clamps a specified number between a min & max
  ------------------------------------------------------------------------------------------------------------------
  
  if (type(num) == "number" and type(min) == "number" and type(max) == "number") then -- if num, min, and max are numbers then
    if (min > max) then -- if programmer puts max number in place of minimum number... don't punish them for it
      local temp = min -- create a temporary value so that 'min' can be stored
      min = max -- set 'min' to 'max'
      max = temp -- set 'max' to the temporary value
    end
    
    if (num < min) then -- if number is smaller than 'min' then
      num = min -- set number to 'min'
    elseif (num > max) then -- if number is greater than 'max' then
      num = max -- set number to 'max'
    end
    -- "if elseif" statement because if it's lower than minimum then it's obviously not going to be greater than maximum and vice versa (and DO NOT clamp if the number is between min and max)
    
    return num -- return the provided number to clamp
  else -- if the programmer doesn't mention all the parameters ):<
    if (type(num) ~= "number") then -- time to ERROR
      error("math.clamp: no number provided to be clamped")
    elseif (type(min) ~= "number") then
      error("math.clamp: no minimum number provided for clamping")
    elseif (type(max) ~= "number") then
      error("math.clamp: no maximum number provided for clamping")
    end
  end
end

function tph_lib.math.round(num,decimal)
  -- PARAMETERS: num;"number" - number to be rounded | decimal;"number"/"integer" (OPTIONAL) - how many decimal placements you want to round only
  -- RETURNS: number - 'num' that is rounded accorddingly
  -- FUNCTION: rounds a specified number, and can round only certain decimal points if specified with decimal (decimal = 2 for example, will make it so it doesn't round 1.256 to 1, rather to 1.26)
  ------------------------------------------------------------------------------------------------------------------
  
  if (type(num) == "number") then
    if (type(decimal) == "number") then -- decimal not necessary to provide (only if you want to conserve certain placements of decimals) but if you specify it...
      if (decimal < 0) then -- decimal can't be lower than 0
        decimal = 0
      end
      
      decimal = math.ceil(decimal) -- no giving a number with a decimal to determine decimal place (integer only)
    else -- no decimal specified, then set it to 0 for the code to use :D (will not conserve any decimal placement)
      decimal = 0
    end
    
    decimal = 10 ^ decimal -- make 'decimal' a number to the power of 10 by it to multiply 'num' by to conserve any wanted decimal points (0 ensures no decimal points for conservation, 1 will leave 1 decimal point and so on)
    
    num = num * decimal -- multiply num by power'd decimal as to conserve wanted decimal points
    
    num = math.floor(num + .5) -- use math.floor to round 'num' (if it's below 0.4, the number will not round up as 0.4 + 0.5 will equal 0.9 and will still be floor'd to 0 while if it's above 0.4 or is 0.5, the number will round up as 0.6 + 0.5 will equal 1.1 and will be the floor'd to 1)
    
    return (num / decimal) -- return 'num' divided by power'd decimal to conserve any wanted decimal points
  else -- if programmer doesn't specify a number to round, then my code is going to be mad
    error("math.round: no number provided")
  end
end

-- "MATH" SECTION FUNCTIONS



-- ALTERNATIVE NAMING

tph_lib.is_pos_valid = tph_lib.pos_is_valid
tph_lib.pos_valid = tph_lib.pos_is_valid

tph_lib.len = tph_lib.table.length
--